#!/bin/bash
for subDir in `ls -d */`
do
	cd $subDir
	sh *.sh
	cd ..
done